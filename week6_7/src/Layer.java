import Shapes.*;

import java.util.ArrayList;
import java.util.List;

//Lớp layer đại diện cho lớp, chứa các Shape
public class Layer {
    private boolean visible;
    private List<Shape> shapeList = new ArrayList<>();



    /** deleteAllTriangle() xóa tất cả các hình vẽ lớp Triangle
     * @param void
     * @return void
     */

    public boolean isVisible() { return this.visible; }



    public boolean isEmpty() {
        return shapeList.isEmpty();
    }
