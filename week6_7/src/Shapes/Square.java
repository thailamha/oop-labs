package Shapes;

public class Square extends Quadrilateral {


    //Getter/Setter cho Side
    public void setSide(double side) {
        this.setSizeX(side); this.setSizeY(side);
    }
    public double getSide() {
        return this.getSizeX();
    }

    /** Hàm toString() trả lại String chứa thuộc tính của đối tượng
     * @param none
     * @return String
     */
    public String toString() {
        String output = String.format("%10s | %10s | %6b | %10.3f %10.3f | %10.3f",this.getName(),this.getColor(),this.isFilled(),this.getPosition().getX(),this.getPosition().getY(),this.getSize().getX());
        return output;
    }
