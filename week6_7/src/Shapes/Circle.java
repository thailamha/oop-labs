package Shapes;

public class Circle extends Shape {
    //PI
    private final double PI = 3.14;

    //Thuộc tính bán kính
    private double radius;

    //Constructors
    public Circle(double radius) {
        defaultInit("Circle");
        this.radius = radius;
    }


    //Getters
    public double getRadius() { return this.radius; }

    //Setters
    public void setRadius(double radius) { this.radius = radius; }

    /** Get diện tính hình
     * @param void
     * @return double diện tính hình
     */
    public double getArea() {
        return (PI * this.radius * this.radius);
    }

    /** Get chu vi hình
     * @param void
     * @return double chu vi hình
     */
    public double getPerimeter() {
        return (PI * 2 * this.radius);
    }


    /**
     * Kiểm tra 2 hình trùng nhau
     * @param circle
     * @return boolean
     */
