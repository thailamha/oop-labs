package Shapes;

import Maths.Vector2;

//Lớp Quadrilateral đại diện cho các hình vẽ có 4 cạnh
abstract class Quadrilateral extends Shape{
    public Vector2 size = new Vector2();

    //Setters
    abstract public void setSizeX(double X);
    abstract public void setSizeY(double Y);

    //Getters
    public Vector2 getSize() { return size; }
    public double getSizeX() { return size.getX(); }
    public double getSizeY() { return size.getY(); }

    /** Get diện tính hình
     * @param void
     * @return double diện tính hình
     */
    public double getArea() {
        return (size.getX() * size.getY());
    }

    /** Get chu vi hình
     * @param void
     * @return double chu vi hình
     */
    public double getPerimeter() {
        return (2 * (size.getX() + size.getY()));
    }
}
