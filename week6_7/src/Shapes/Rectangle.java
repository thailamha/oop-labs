package Shapes;

//Lớp Rectangle đại diện cho các hình chữ nhật
public class Rectangle extends Quadrilateral {
    //Constructor
    public Rectangle(String name, double sizeX, double sizeY) {
        this.getSize().set(sizeX,sizeY);
    }
    public Rectangle(double posX, double posY, double sizeX, double sizeY) {
        this.getSize().set(sizeX,sizeY);
    }

    //Setters
    public void setSizeX(double X) {
        this.getSize().setX(X);
    }
    public void setSizeY(double Y) {
        this.getSize().setY(Y);
    }

