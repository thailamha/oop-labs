package Shapes;

public class Hexagon extends Shape{
    public double side;

    /**
     * Constructor
     */

    public Hexagon(String name) {
        init(name);
        setPosition(0,0);
        this.side = 1;
    }

    public Hexagon(String name, double side) {
        setPosition(0,0);
        this.side = side;
    }

    //Setter
    public void setSide(double side) { this.side = side; }

    //Getter
    public double getSide() { return this.side; }

    /** Trả lại diện tích của hình
     * @param void
     * @return void
     */
    public void getArea() {

    }

    /** Trả lại chu vi của hình
     * @param void
     * @return void
     */
    public double getPerimeter() {
        return side * 5;
    }

    /**
     * Kiểm tra 2 hình trùng nhau
     * @param hex
