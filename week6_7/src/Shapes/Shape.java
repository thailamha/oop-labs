package Shapes;

import Maths.Vector2;

//Lớp Shape đại diện cho các hình vẽ
public class Shape {

    //Setters
    public void setName(String name) {
        this.name = name;
    }
    public void setColor(String color) {
        this.color = color;
    }
    public void setFilled(boolean filled) {
        this.filled = filled;
    }
    public void setPosition(double x, double y) {
        this.position.set(x,y);
    }

    //Getters
    public String getName() { return this.name; }
    public String getColor() { return this.color; }
    public boolean isFilled() { return this.filled; }
    public Vector2 getPosition() { return this.position; }

