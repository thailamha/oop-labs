package Shapes;

//Lớp Triangle đại diện cho hình tam giác
public class Triangle extends Shape {
    private double A, B ,C;

    //Constructor
    public Triangle() {
        defaultInit("Triangle");
        this.A = 0; this.B = 0; this.C = 0;
    }
    public Triangle(String name) {
        defaultInit(name);
        this.A = 5; this.B = 4; this.C = 3;
    }
    public Triangle(double A, double B, double C) {
        defaultInit("Triangle");
        if (checkTriangle(A,B,C)) {
            this.A = A; this.B = B; this.C = C;
        } else {
            System.out.println("Not a triangle.");
            this.A = 0; this.B = 0; this.C = 0;
        }
        this.setPosition(0,0);
    }
    public Triangle(double A, double B, double C, double posX, double posY) {
        defaultInit("Triangle");
        if (checkTriangle(A,B,C)) {
            this.A = A; this.B = B; this.C = C;
        } else {
            System.out.println("Not a triangle.");
            this.A = 0; this.B = 0; this.C = 0;
        }
        this.setPosition(posX, posY);
    }

    //Setters
    public void setTriangle(double A, double B, double C) {
        if (checkTriangle(A,B,C)) {
            this.A = A; this.B = B; this.C = C;
        } else {
            System.out.println("Not a triangle.");
        }
    }
    //Getters
    public double getA() { return this.A; }
    public double getB() { return this.B; }
    public double getC() { return this.C; }
    /** Get chu vi hình
     * @param void
     * @return double chu vi hình
     */
    public double getPerimeter() {
        return this.A + this.B + this.C;
    }

    /** Get diện tính hình
     * @param void
     * @return double diện tính hình
     */
    public double getArea() {
        double s = 0.5 * (this.A + this.B + this.C);
        double output = Math.sqrt(s * (s - this.A) * (s - this.B) * (s - this.C));
        return output;
    }

    /**
     * So sánh cạnh của 2 Triangle
     * @param triangle
     * @return
     */
    public boolean compareSides(Triangle triangle) {
        if (this.A == triangle.getA() && this.B == triangle.getB() && this.C == triangle.getC()) {
            return true;
        } else {
            return false;
        }
    }

    /** checkTriangle() kiểm tra xem 3 thông số cạnh có hợp lệ là 1 tam giác hay không
     * @param double A,B,C là 3 cạnh của tam giác
     * @return boolean true/false
     */
    private boolean checkTriangle(double A, double B, double C) {
        if (A + B <= C || B + C <= A || A + C <= B) {
            return true;
        } else {
            return false;
        }
    }

    /** Hàm toString() trả lại String chứa thuộc tính của đối tượng
     * @param none
     * @return String
     */
    public String toString() {
        String output = String.format("%10s | %10s | %6b | %10.3f %10.3f | %8.3f %8.3f %8.3f",this.getName(),this.getColor(),this.isFilled(),this.getPosition().getX(),this.getPosition().getY(),this.A,this.B,this.C);
        return output;
    }

    /**
     * Kiểm tra 2 hình trùng nhau
     * @param triangle
     * @return boolean
     */
