package cau2;

/**
 * Class Circle
 *
 * @author Lam Ha Thai
 * @version 1.0
 * @since 2018-10-01
 */
public class Circle extends Shape {
    static final double PI = 3.14;
    private double radius = 1.0;

    /**
     * constructor
     */
    public Circle() {
    }

    public Circle(double radius) {
        this.radius = radius;
    }


    /**
     * getter & setter
     */
    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }


    public double getArea() {
        return PI * Math.pow(this.getRadius(), 2);
    }

    public double getPerimeter() {
        return PI * this.getRadius() * 2;
    }

    /**
     * main method to test class
     */
    public static void main(String[] args) {
        Circle circle = new Circle();
        System.out.println(circle.toString());
    }
}
