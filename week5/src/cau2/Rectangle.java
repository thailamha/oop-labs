package cau2;

/**
 * Class Retangle
 *
 * @author Lam Ha Thai
 * @version 1.0
 * @since 2018-10-01
 */
public class Rectangle extends Shape {

    /**
     * constructor
     */
    public Rectangle() {
        width = 1.0;
    }

    public Rectangle(double width, double length) {
        this.length = length;
    }


    public void setWidth(double width) {
        this.width = width;
    }


    public void setLength(double length) {
        this.length = length;
    }

    /**
     * calculate area
     *
     * @param No
     * @return area
     */
    public double getArea() {
        return this.getWidth() * this.getLength();
    }

    /**
     * calcalate perimeter
     *
     * @param no
     * @return perimeter
     */
    public double getPerimeter() {
        return (this.getWidth() + this.getLength()) * 2;
    }

    @Override
    public String toString() {
        return super.toString() + " |Area: " + this.getArea() + " |Perimeter: " + this.getPerimeter();
    }

    /**
     * main method to test class
     */
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle();
        System.out.println(rectangle.toString());
    }

}
