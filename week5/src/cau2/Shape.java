/**
 * Class Shape
 *
 * @author Lam Ha Thai
 * @version 1.0
 * @since 2018-10-01
 */
package cau2;

public class Shape {


    /**
     * constructor
     */
    public Shape() {
        color = "red";
        filled = true;
    }

    public Shape(String color, boolean filled) {
        this.color = color;
        this.filled = filled;
    }

    /**
     * getter & setter
     */
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFilled() {
        return filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    @Override
    public String toString() {
        return "Color: " + this.getColor() + " |Filled: " + this.isFilled();
    }

    /**
     * main method to test class
     */
    public static void main(String[] args) {
        Shape shape = new Shape();
        System.out.println(shape.toString());
    }

}
