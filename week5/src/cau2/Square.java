package cau2;

/**
 * Class Square
 *
 * @author Lam Ha Thai
 * @version 1.0
 * @since 2018-10-01
 */
class Square extends Rectangle {

    /**
     * constructor
     */

    public Square() {
        super() ;
    }



    /**
     * getter & setter
     */
    public double getSide() {
        return super.getLength();
    }

    public void setSide(double side) {
        super.setLength(side);
        super.setWidth(side);
    }

    @Override
    public void setWidth(double side) {
        super.setWidth(side);
    }

    @Override
    public void setLength(double side) {
        super.setLength(side);
    }


    /**
     * main method to test class
     */
    public static void main(String[] args) {
        Square square = new Square();
        System.out.println(square.toString());
    }

}

