package cau1;

/**
 * Class HoaQua
 *
 * @author Lam Ha Thai
 * @version 1.0
 * @since 2018-10-01
 */
class HoaQua {

    protected double giaban, cannang;
    protected int ngaynhap, soluong;
    protected String nguongoc;

    /**
     * constructor
     */
    public HoaQua() {
    }

    public HoaQua(double giaban, double cannang, int ngaynhap, int soluong, String nguongoc) {
        this.giaban = giaban;
        this.cannang = cannang;
        this.ngaynhap = ngaynhap;
        this.soluong = soluong;
        this.nguongoc = nguongoc;
    }

    /**
     * getter & setter
     */
    public double getGiaban() {
        return giaban;
    }

    public void setGiaban(double giaban) {
        this.giaban = giaban;
    }

    public double getCannang() {
        return cannang;
    }

    public void setCannang(double cannang) {
        this.cannang = cannang;
    }

    public int getNgaynhap() {
        return ngaynhap;
    }

    public void setNgaynhap(int ngaynhap) {
        this.ngaynhap = ngaynhap;
    }

    public int getSoluong() {
        return soluong;
    }

    public void setSoluong(int soluong) {
        this.soluong = soluong;
    }

    public String getNguongoc() {
        return nguongoc;
    }

    public void setNguongoc(String nguongoc) {
        this.nguongoc = nguongoc;
    }

    public String toString() {
        return "Giá bán: " + this.getGiaban() + " |Cân nặng: " + this.getCannang() + " |Số lượng: " + this.getSoluong() + " |Ngày nhập: " + this.getNgaynhap() + " |Nguồn gốc: " + this.getNguongoc();
    }

    /**
     * main method to test class
     */
    public static void main(String[] args) {
        HoaQua hoaqua = new HoaQua();
        hoaqua.setGiaban(50000);
        hoaqua.setCannang(10);
        hoaqua.setSoluong(100);
        hoaqua.setNgaynhap(5);
        hoaqua.setNguongoc("Đà Lạt");
        System.out.println(hoaqua.toString());

    }

}
