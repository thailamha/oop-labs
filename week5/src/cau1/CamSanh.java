package cau1;
/**
 * Class CamSanh
 * @author Lam Ha Thai
 * @since   2018-10-01
 * @version 1.0
 *
 */
public class CamSanh extends QuaCam{
	/**
	 * constructor
	 */
	public CamSanh() {}

	/**
	 * getter & setter
	 * */

	@Override
	/**
	 * main method to test class
	 * */	public static void main(String[] args) {
		CamSanh camsanh= new CamSanh();
		camsanh.setGiaban(200000);
		camsanh.setCannang(10);
		camsanh.setSoluong(25);
		camsanh.setNgaynhap(5);

		System.out.println(camsanh.toString());
	}
}
