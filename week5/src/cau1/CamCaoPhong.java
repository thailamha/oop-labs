/**
 * Class CamCaoPhong
 * @author Lam Ha Thai
 * @since   2018-10-01
 * @version 1.0
 *
 */
package cau1;

public class CamCaoPhong extends QuaCam{
	private String diemdacbiet;
	/**
	 * constructor
	 */
	public CamCaoPhong() {}
	public CamCaoPhong(double giaban, double cannang, int ngaynhap, int soluong, String nguongoc, String huongvi, String noitrong, String ten, String diemdacbiet) {
		super(giaban, cannang, ngaynhap, soluong, nguongoc, huongvi, ten);
		this.diemdacbiet= diemdacbiet;
	}
	/**
	 * getter & setter
	 * */
	public String getDiemdacbiet() {
		return diemdacbiet;
	}

	public void setDiemdacbiet(String diemdacbiet) {
		this.diemdacbiet = diemdacbiet;
	}
	@Override
	public String toString() {
		return super.toString()+" \n Điểm đặc biệt: "+this.getDiemdacbiet();
	}
	/**
	 * main method to test class
	 * */
	public static void main(String[] args) {
		CamCaoPhong camcaophong= new CamCaoPhong();
		camcaophong.setGiaban(200000);
		camcaophong.setCannang(10);
		camcaophong.setSoluong(25);
		camcaophong.setNgaynhap(5);
		camcaophong.setNguongoc("Cao Phong");
		camcaophong.setTen("Quả cam");
		camcaophong.setHuongvi("Ngọt mát");
		camcaophong.setDiemdacbiet("Không hạt");
		camcaophong.setTen("Cam Cao Phong");
		System.out.println(camcaophong.toString());
	}
}
