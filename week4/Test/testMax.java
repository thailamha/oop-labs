/**
 * Unit Test ham tim so lon hon trong 2 so
 *
 * @author Lam Ha Thai
 * @version 1.0
 * @since 2018-09-25
 */

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class testMax {
    week4 test = new week4();

    /**
     * Cac ham test
     * */
    @Test
    public void testMax() {
        assertEquals("test", 20, test.max(15, 20));

    }

    @Test
    public void testMax1() {
        assertEquals("test", 22, test.max(12, 22));
    }

    @Test
    public void testMax2() {
        assertEquals("test", 2, test.max(1, 2));
    }

    @Test
    public void testMax3() {
        assertEquals("test", 100, test.max(100, 20));
    }

    @Test
    public void testMax4() {
        assertEquals("test", 0, test.max(-1, 0));
    }
}
