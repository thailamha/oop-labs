/**
 * Unit Test ham tinh BMI
 * @author Lam Ha Thai
 * @since   2018-09-25
 * @version 1.0
 */

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class testBMI {
    week4 test = new week4();
    /**
     * Cac ham test
     * */
    @Test
    public void testBMI() {
        assertEquals("Thiếu cân", test.calBMI(45, 1.8));    }

    @Test
    public void testBMI1() {
        assertEquals("Thừa cân", test.calBMI(80, 1.8));
    }

    @Test
    public void testBMI2() {
        assertEquals("Bình thường", test.calBMI(50, 1.55));
    }

    @Test
    public void testBMI3() {
        assertEquals("Bình thường", test.calBMI(56, 1.60));
    }

    @Test
    public void testBMI4() {
        assertEquals("Béo phì", test.calBMI(120, 1.50));
    }
}
