/**
 *Unit Test ham tim Min trong mang
 * @author Lam Ha Thai
 * @since   2018-09-25
 * @version 1.0
 */

import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class testMinInArray {
    week4 test = new week4();
 /**
  * Cac ham test
  * */
    @Test
    public void test() {
        int[] arr= {3, 4, 5, 6, 6};
        assertEquals(3, test.minInArray(arr));
    }
    @Test
    public void test1() {
        int[] arr= {3, 4};
        assertEquals(3, test.minInArray(arr));
    }
    @Test
    public void test2() {
        int[] arr= {55, 56, 57};
        assertEquals(55, test.minInArray(arr));
    }
    @Test
    public void test3() {
        int[] arr= {2, 3, 4, 5, 6, 7, 8, 9, 10, 13, 14, 15};
        assertEquals(2, test.minInArray(arr));
    }
    @Test
    public void test4() {
        int[] arr= {6};
        assertEquals(6, test.minInArray(arr));
    }
}
