/**
 * HelloWorld is a function to print Hello World, Java!
* @author Lam Ha Thai
* @since   2018-09-04
* @version 1.0
*/

public class HelloWorld {

    public static void main(String[] args) {
        // print Hello World, Java! to terminal windows
        System.out.println("Hello World, Java!");
        
    }

}
