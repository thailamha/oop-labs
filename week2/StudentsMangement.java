/**
 * StudentsMangement is a class to manage students 
 * @author Lam Ha Thai
 * @since   2018-09-07
 * @version 1.0
 * 
 */

public class StudentsMangement {
	public static Student[] students = new Student[100];
    public static void main(String[] args){
       students[0] =  new Student("lam ha thai", "17021333", "K59CLC", "asd@gmail.com");
       students[1] =  new Student("lam ", "17021233", "K59CLC", "ad@gmail.com");
       students[2] = new Student("ngo as dfkj", "17029873","K59CB", "dskjfk@kjh.com");
      System.out.println(students.length);
       if (sameGroup(students[0],students[1])) {
       	System.out.println("yes");
       } else {System.out.println("no");}
       studentByGroup(students);
       System.out.println(students[100].getInfo());
    }
    // check if two student in the same group
    public static boolean sameGroup(Student s1, Student s2){
        return s1.getGroup() == s2.getGroup(); 
    }
    // print all student in the same group
    public static void studentByGroup(Student[] students){
    	// for (int i=0; i < students.length; i++){
    	// 	if (students[i].getGroup()==group){
    	// 		System.out.println(students[i].getInfo());
    	// 	}
    	// }
    	boolean[] check = new boolean[100] ;
    	for (int i = 0; i < students.length; i++){
    		if (check[i] == false) {
    			for (int j = i; j < students.length; j++) {
    				if ( students[i].getGroup() == students[j].getGroup()) {
    					System.out.println(students[j].getInfo());
    					check[j] = true;
    				}
    			}
    		}
    	}
    }
    // remove student by id 
    public static void removeStudentById(Student[] students, String id){
    	for (int i = 0; i< students.length; i++){
    		if (students[i].getId()==id) {
    			  for(int j = i; j < students.length - 1; j++){
                    students[j] = students[j+1];
                }
                break;
    		}
    	}
    }
}