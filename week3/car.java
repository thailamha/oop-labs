
/**
 * StudentsMangement is a class to manage students
 * @author Lam Ha Thai
 * @since   2018-09-15
 * @version 1.0
 *
 */

class car {
	private int yearModel;
	private String madeBy;
	private int speed;

    //--------------------------------
	// getter and setter
	public int getSpeed() {
		return speed;
	}

	public int getYearModel() {
		return yearModel;
	}

	public String getMadeBy() {
		return madeBy;
	}

	public void setMadeBy(String madeBy) {
		this.madeBy = madeBy;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public void setYearModel(int yearModel) {
		this.yearModel = yearModel;
	}
    //---------------------------------
    //constructor
    public car(){

    }

    //---------------------------------
    // function
    // speed up
	public void accelerate(){
		speed += 5;
	}
    // break
	public void break(){
		speed -= 5;
	}
}