/**
 * StudentsMangement is a class to manage students
 * @author Lam Ha Thai
 * @since   2018-09-16
 * @version 1.0
 *
 */

class book{
    String tittle;
    String author;
    int totalPage;
    int currentPage;
    // constructor
    public book(){

    }
    public book(String tittle, String author, int totalPage){
        this.author = author;
        this.totalPage = totalPage;
        this.tittle = tittle;
        this.currentPage = 1;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public String getAuthor() {
        return author;
    }

    public String getTittle() {
        return tittle;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }
    // read next page
    public void nextPage(){
        this.currentPage = this.currentPage++;
    }
    // read previus page
    public void previusPage(){
        this.currentPage = this.currentPage++;
    }
}