/**
 * Student is a class include student info, method to get and set student info
 * @author Lam Ha Thai
 * @since   2018-09-15
 * @version 1.0
 */

public class Student{

    // declare student info variable
    private String name;
    private String id;
    private String group;
    private String email;

    // getter and setter
    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getId(){
        return id;
    }

    public void setId(String id){
        this.id = id;
    }

    public String getGroup(){
        return group;
    }

    public void setGroup(String group){
        this.group = group;
    }

    public String getEmail(){
        return email;
    }

    public void setEmail(String email){
        this.email = email;
    }

    /* contructor Student with 4 params
     * @params name is name of student
     * @params id is id of student
     * @params group is group of student
     * @params email is email of student
     */
    public Student(String name, String id, String group, String email){
        this.name = name;
        this.id = id;
        this.group = group;
        this.email = email;
    }

    // constructor Student without params
    public Student(){
        this.name = "Student";
        this.id = "000";
        this.group = "K59CB";
        this.email = "uet@vnu.edu.vn";
    }
    // constructor Student without 3 params
    public Student(String n, String sid, String ema){
        this.name = n;
        this.id = sid;
        this.group = "K59CB";
        this.email = ema;
    }

    // function copy student
    public Student(Student s){
        this.name = s.name;
        this.id = s.id;
        this.group = s.group;
        this.email = s.email;
    }

    // function return all student info
    public String getInfo() {
        return  "name: " + this.name + "\n"+"id: " + this.id +"\n"+"group: " + this.group +"\n"+"email: " + this.email+"\n";
    }
}
