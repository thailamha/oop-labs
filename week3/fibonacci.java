/**
 * StudentsMangement is a class to manage students
 *
 * @author Lam Ha Thai
 * @version 1.0
 * @since 2018-09-13
 */

import java.util.Scanner;
import java.io.*;

class fibonacci {
    // print n fibonacci number
    public static void fibonacci(int n) {
        int t1 = 0, t2 = 1;
        for (int i = 1; i <= n; ++i) {
            System.out.print(t1 + " ");

            int sum = t1 + t2;
            t1 = t2;
            t2 = sum;
        }
    }

    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        fibonacci(n);
    }
}  