/**
 * StudentsMangement is a class to manage students
 * @author Lam Ha Thai
 * @since   2018-09-15
 * @version 1.0
 *
 */

public class product {

    String name;
    double price;
    int quantity;
    //---------------------------------
    // constructor
    public product(){

    }
    public product(String name, double price){
        this.name = name;
        this.price = price;
    }
    //------------------------------
    // getter and setter

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    //---------------------------------

    // print product info
    public void printInfo() {
        System.out.println("Product: "+name+", Price: "+price+", Quantity: "+quantity);
    }
}