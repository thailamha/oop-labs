/**
 * StudentsMangement is a class to manage students 
 * @author Lam Ha Thai
 * @since   2018-09-17
 * @version 1.0
 *
 */

class PS {
    private int numberator, denomirator;

    //---------------------------------
    // getter and setter
    public int getNumberator() {
        return numberator;
    }

    public void setNumberator(int a) {
        this.numberator = a;
    }

    public int getDenominator() {
        return denomirator;
    }

    public void setDenominator(int b) {
        this.denomirator = b;
    }

    // constructor
    public PS(int a, int b) {
        this.numberator = a;
        this.denomirator = b;
    }
    /* tim uoc chung lon nhat
    * @params a, b
    * @return uoc chung lon nhat cua a va b
    * */
    public static int gcd(int a, int b)
    {
        return (b == 0) ? a : gcd(b, a % b);
    }
    // toi gian phan so
    //@return phan so da duoc toi gian
    public PS minimal() {
        int k = gcd(this.getNumberator(), this.getDenominator());
        this.setNumberator(this.getNumberator() / k);
        this.setDenominator(this.getDenominator() / k);
        return this;
    }
    /* cong hai phan so
    *@param phan so b
    * @return phan so sau khi cong them phan so b
    **/
    public PS addition(PS b) {
        this.setNumberator(this.getNumberator() * b.getDenominator() + this.getDenominator() * b.getNumberator());
        this.setDenominator(this.getDenominator() * b.getDenominator());
        return this.minimal();
    }
    /* tru hai phan so
     *@param phan so b
     * @return phan so sau khi tru di phan so b
     **/    public PS subtraction(PS b) {
        this.setNumberator(this.getNumberator() * b.getDenominator() - this.getDenominator() * b.getNumberator());
        this.setDenominator(this.getDenominator() * b.getDenominator());
        return this.minimal();
    }
    /* cong hai phan so
     *@param phan so b
     * @return phan so sau nhan voi phan so b
     **/
    public PS multiplication(PS b) {
        this.setNumberator(this.getNumberator() * b.getNumberator());
        this.setDenominator(this.getDenominator() * b.getDenominator());
        return this.minimal();
    }
    /* cong hai phan so
     *@param phan so b
     * @return phan so sau chia cho phan so b
     **/
    public PS division(PS b) {
        this.setDenominator(this.getNumberator() * b.getDenominator());
        this.setNumberator(this.getDenominator() * b.getNumberator());
        return this.minimal();
    }
    // in phan so
    public void printFraction() {
        System.out.print(this.getNumberator());
        System.out.println(this.getDenominator());
    }
    /* so sanh hai phan so
     *@param phan so b
     * @return true neu phan so bang phan so b
     * @return false neu phan so khong bang phan so b
     **/
    public boolean equals(PS p) {
        PS b = this.minimal();
        p = p.minimal();
        return (this.getNumberator() == p.getNumberator() && this.getDenominator() == p.getDenominator());
    }
    /*
    * ham main de thuc thi cac ham lien quan den phan so
    * @param args khong dung
    * @return ham main khong tra ve gia tri nao
    * */
    public static void main(String[] args) {
        PS a = new PS(2, 3);
        PS c = new PS(1, 3);
        PS d = new PS(2,3);
        PS e = new PS(2,3);

        a.division(c);
        a.printFraction();
        a.addition(c);
        a.printFraction();
        a.subtraction(c);
        a.printFraction();
        a.multiplication(c);
        a.printFraction();
        System.out.println(e.equals(d));


    }
}