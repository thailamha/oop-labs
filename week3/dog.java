class dog{
    int size;
    String name;

    // getter and setter
    public String getName() {
        return name;
    }

    public int getSize() {
        return size;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSize(int size) {
        this.size = size;
    }
    // constructor
    public dog(){

    }
    public dog(String name, int size){
        this.name = name;
        this.size = size;
    }

    // make a noise
    public void makeANoise(){
        System.out.println("Ruff!!");
    }
}