
/**
 * StudentsMangement is a class to manage students
 * @author Lam Ha Thai
 * @since   2018-09-13
 * @version 1.0
 *
 */
class Circle {
    point center;
    double radius;
    //constructor
    public Circle(){}

    public Circle(point center,  double radius, String color, boolean filled) {
        this.center = center;
        this.radius = radius;
        this.color = color;
        this.filled = filled;
    }
    // getter & setter
    public double getRadius() {

        return radius;
    }

    public void setRadius(double radius) {

        this.radius = radius;
    }

    public point getCenter()
    {
        return center;
    }

    public void setCenter(point center) {
        this.center = center;
    }



}
