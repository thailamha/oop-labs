/**
 * StudentsMangement is a class to manage students
 * @author Lam Ha Thai
 * @since   2018-09-16
 * @version 1.0
 *
 */

public class Mobile {
    private String manufacturer;
    private String operating_system;
    public String model;
    private int price;

    //---------------------------------
    // constructor
    Mobile(String man, String o,String m, int p){
        this.manufacturer = man;
        this.operating_system=o;
        this.model=m;
        this.price=p;
    }
    //---------------------------------
    // getter & setter
    public String getModel(){
        return this.model;
    }

    public int getPrice() {
        return price;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public String getOperating_system() {
        return operating_system;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setOperating_system(String operating_system) {
        this.operating_system = operating_system;
    }
    //---------------------------------
    // function

    // ringing the mobile
    public void ringing(){
        System.out.prinln("the phone is ringing");
    }
    // make a call
    public void makeACall(){
        System.out.println("connecting..........");
    }
}