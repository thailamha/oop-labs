
/**
 * StudentsMangement is a class to manage students 
 * @author Lam Ha Thai
 * @since   2018-09-015
 * @version 1.0
 * 
 */

public class Employee {

   String name;
   int age;
   double salary;

   // constructor
   public Employee(String name) {

       this.name = name;
   }

   // getter and setter
   public double getSalary() {
      return salary;
   }

   public int getAge() {
      return age;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public void setSalary(double salary) {
      this.salary = salary;
   }

   public void setAge(int age) {
      this.age = age;
   }

    //---------------------------------
    // function
   // Print the Employee details
   public void printEmployee() {
      System.out.println("Name:"+ name );
      System.out.println("Age:" + age );
      System.out.println("Salary:" + salary);
   }
}