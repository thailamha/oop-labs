/**
 * StudentsMangement is a class to manage students
 * @author Lam Ha Thai
 * @since   2018-09-12
 * @version 1.0
 *
 */

import java.util.Scanner;
import java.io.*;

class ucln{
	/*
	* @param a, b
	* @return uoc chung lon nhat cua a va b
	* */
	public static int gcd(int a, int b){

		return (b == 0) ? a : gcd(b, a % b);
	}
	
	public static void main(String[] args){
		int a, b;
		Scanner sc = new Scanner(System.in);
		a = sc.nextInt();
		b = sc.nextInt();
		System.out.println(gcd(a, b));
	}
}