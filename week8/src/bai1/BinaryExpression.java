package bai1;

/**
 * abstract class BinaryExpression
 *
 * @author Lam Ha Thai
 * @version 1.0
 * @since 2018-10-23
 */
public abstract class BinaryExpression extends Expression {
    /**
     * Ham in bieu thuc
     *
     * @return String
     */
    @Override
    public abstract String toString();

    /**
     * Ham tinh gia tri bieu thuc
     *
     * @return int
     */
    @Override
    public abstract int evaluate();
}