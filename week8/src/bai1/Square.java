package bai1;

/**
 * Square class
 *
 * @author Lam Ha Thai
 * @version 1.2
 * @since 2018-10-23
 */

import static java.lang.Math.pow;

public class Square extends Expression {
    public Expression expression;

    /**
     * constructor
     * @param e bieu thuc
     */
    public Square(Expression e) {
        expression = e;
    }

    /**
     * Ham in bieu thuc
     *
     * @return String
     */
    @Override
    public String toString() {

    }

    /**
     * Ham tinh gia tri bieu thuc
     *
     * @return int
     */
    @Override
    public int evaluate() {
        return (int) pow(expression.evaluate(), 2);
    }
}