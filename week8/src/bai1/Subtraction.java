package bai1;

/**
 * class Subtraction
 *
 * @author Lam Ha Thai
 * @version 1.0
 * @since 2018-10-23
 */
public class Subtraction extends BinaryExpression {
    public Expression _left;
    public Expression _right;
    /**
     * constructor
     *
     * @param l bieu thuc ve trai
     * @param r bieu thuc ve phai
     */
    public Subtraction(Expression l, Expression r) {
        _left = l;
        _right = r;
    }


    /**
     * Ham tinh gia tri bieu thuc
     *
     * @return int
     */
    @Override
    public int evaluate() {
        return _left.evaluate() - _right.evaluate();
    }
}