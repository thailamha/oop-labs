package bai1;

/**
 * class Division
 *
 * @author Lam Ha Thai
 * @version 1.0
 * @since 2018-10-23
 */
public class Division extends BinaryExpression {

    /**
     * Ham in bieu thuc
     *
     * @return String
     */

    /**
     * Ham tinh gia tri bieu thuc
     *
     * @return int
     */
    @Override
    public int evaluate() {
        return _left.evaluate() / _right.evaluate();
    }
    /**
     * constructor
     *
     * @param l bieu thuc ve trai
     * @param r bieu thuc ve phai
     */
    public Division(Expression l, Expression r) {
        _left = l;
        _right = r;
    }

}