package bai1;
/**
 * class Numeral
 *
 * @author Lam Ha Thai
 * @version 1.0
 * @since 2018-10-23
 */


public class Numeral extends Expression {

    /**
     * getter and setter
     */
    public int getV() {
        return v;
    }

    public void setV(int v) {
        this.v = v;
    }

    /**
     * constructor
     */
    public Numeral() {
    }

    ;

    /**
     * constructor
     *
     * @param val gia tri
     */
    public Numeral(int val) {
        v = val;
    }

    /**
     * Ham in bieu thuc
     *
     * @return String
     */
    @Override
    public String toString() {
        return String.valueOf(v);
    }

    /**
     * Ham tinh gia tri bieu thuc
     *
     * @return int
     */
    @Override
    public int evaluate() {
        return v;
    }
}

