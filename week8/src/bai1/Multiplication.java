package bai1;

/**
 * class Multiplication
 *
 * @author Lam Ha Thai
 * @version 1.0
 * @since 2018-10-23
 */
public class Multiplication extends BinaryExpression {

    public Multiplication(Expression l, Expression r) {
        _left = l;
        _right = r;
    }


    /**
     * Ham tinh gia tri bieu thuc
     *
     * @return int
     */
    @Override
    public int evaluate() {
        return _left.evaluate() * _right.evaluate();
    }
}