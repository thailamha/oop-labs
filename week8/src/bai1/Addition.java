package bai1;

/**
 * Addition class
 *
 * @author Lam Ha Thai
 * @version 1.0
 * @since 2018-10-23
 */
public class Addition extends BinaryExpression {


    @Override
    public int evaluate() {
        return _left.evaluate() + _right.evaluate();
    }

    public Addition(Expression l, Expression r) {
        _left = l;
        _right = r;
    }

}