package bai1;

/**
 * abstract class Expression
 *
 * @author Lam Ha Thai
 * @version 1.0
 * @since 2018-10-23
 */
public abstract class Expression {

    /**
     * print expression
     */
    public abstract String toString();
    /**
     * calculate expression
     */
    public abstract int evaluate();

}
