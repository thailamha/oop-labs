package bai2;
/**
 *  class IoException
 *
 * @author Lam Ha Thai
 * @version 1.0
 * @since 2018-10-23
 */
public class IoException {
    public static void printLine(String line) throws Exception {
        System.out.printf(line);
    }
    public static void main(String[] args) throws Exception {
        printLine("a");
        printLine(null);
    }
}
