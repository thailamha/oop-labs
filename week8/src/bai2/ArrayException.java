package bai2;

import java.util.ArrayList;

/**
 * class ArrayException
 *
 * @author Lam Ha Thai
 * @version 1.0
 * @since 2018-10-23
 */
public class ArrayException {

    public static void main(String[] args) throws Exception {
        ArrayList a = new ArrayList();
        a.add(10);
        printElement(a, 0);
        printElement(a, 1);
    }
}
