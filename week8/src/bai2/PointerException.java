package bai2;

/**
 * class PointerException
 *
 * @author Lam Ha Thai
 * @version 1.0
 * @since 2018-10-23
 */
public class PointerException {
    public static void printString(String s) throws Exception {
        System.out.println(s);
    }

    public static void main(String[] args) throws Exception {
        printString("a");
        printString(null);
    }
}
