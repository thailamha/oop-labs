package bai2;

/**
 * class ArithmeticException
 *
 * @author Lam Ha Thai
 * @version 1.0
 * @since 2018-10-23
 */
public class ArithmeticException {
    /**
     * ham chia 2 so
     *
     * @param a so thu 1
     * @param b so thu 2
     * @throws Exception
     */
    public static void division(int a, int b) throws Exception {
        if (b == 0) throw new Exception("khong the chia cho 0");
        System.out.print(a / b);
    }

    public static void main(String[] args) throws Exception {
        division(10, 3);
        division(10, 0);
    }
}
