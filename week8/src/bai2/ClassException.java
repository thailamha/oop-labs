package bai2;

/**
 * class ClassException
 *
 * @author Lam Ha Thai
 * @version 1.0
 * @since 2018-10-23
 */
public class ClassException {
    public static boolean intToBoolean(int n) throws Exception {
        return (n == 1);
    }

    public static void main(String[] args) throws Exception {
        intToBoolean(1);
        intToBoolean(20);

        intToBoolean(-1);

    }
}
